// Importamos las librerias
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

// Creamos la app
const app = express();

// Establecemos la ruta del servidor
var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// Analizamos las peticiones content-type - application/json
app.use(bodyParser.json());

// Analizamos las peticiones content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./app/models");

db.sequelize.sync();
// Para depurar
// db.sequelize.sync({ force: true }).then(() => {
//   console.log("Drop and re-sync db.");
// });

// Definimos una ruta principal mediante GET
app.get("/", (req, res) => {
  res.json({ message: "Bienvenido al Contacto de Soporte" });
});

// require("./app/routes/turorial.routes")(app);
require("./app/routes/api_client.routes")(app);

// Establecemos el puerto para escuchar las peticiones
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
  console.log(`El Servidor se ejecuta en el puerto : ${PORT}.`);
});
