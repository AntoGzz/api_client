module.exports = app => {
  const fichas = require("../controllers/api_client.controller.js");

  var router = require("express").Router();

  // Crear una nueva Ficha
  router.post("/", fichas.create);

  // Ver todas las Fichas
  router.get("/", fichas.findAll);

  // Ver todas las Fichas completadas
  router.get("/completed", fichas.findAllCompleted);

  // Ver una Ficha por su ID
  router.get("/:id", fichas.findOne);

  // Actualizar una Ficha por su ID
  router.put("/:id", fichas.update);

  // Eliminar una Ficha por su ID
  router.delete("/:id", fichas.delete);

  // Eliminar todas las Fichas
  router.delete("/", fichas.deleteAll);

  app.use('/api/fichas', router);
};
