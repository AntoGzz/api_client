const db = require("../models");
const Ficha = db.fichas;
const Op = db.Sequelize.Op;

// Creamos y guardamos una nueva Ficha
exports.create = (req, res) => {
  // Validamos la peticion
  if (!req.body.full_name || !req.body.enter_name || !req.body.email || !req.body.pnumber || !req.body.category || !req.body.note) {
    res.status(400).send({
      message: "El Contenido no puede estar Vacio!",
    });
    return;
  }

  // Creamos una Ficha
  const ficha = {
    full_name: req.body.full_name,
    enter_name: req.body.enter_name,
    email: req.body.email,
    pnumber: req.body.pnumber,
    category: req.body.category,
    note: req.body.note,
    completed: req.body.completed ? req.body.completed : false,
  };

  // Guardamos la Ficha en la db
  Ficha.create(ficha)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Ocurrio un error mientras se creaba la Ficha.",
      });
    });
};

// Obtenemos todas las Fichas de la db.
exports.findAll = (req, res) => {
  const full_name = req.query.full_name;
  var condition = full_name
    ? { full_name: { [Op.like]: `%${full_name}%` } }
    : null;

  Ficha.findAll({ where: condition })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error mientras se obtenian las Fichas.",
      });
    });
};

// Buscar una Ficha con un ID
exports.findOne = (req, res) => {
  const id = req.params.id;

  Ficha.findByPk(id)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error obteniendo la Ficha con ID = " + id,
      });
    });
};

// Actualizar una Ficha por el ID en la peticion
exports.update = (req, res) => {
  const id = req.params.id;

  // Tutorial.update(req.body, {
  Ficha.update(req.body, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "La Ficha se actualizo con exito.",
        });
      } else {
        res.send({
          message: `No se puede actualizar la Ficha con ID = ${id}. Quizas la Ficha no se encontro o req.body esta vacio!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error actualizando la Ficha con ID = " + id,
      });
    });
};

// Borrar una Ficha con el ID especifico en la peticion
exports.delete = (req, res) => {
  const id = req.params.id;

  Ficha.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "La Ficha se a borrado con exito!",
        });
      } else {
        res.send({
          message: `No se puede borrar la Ficha con ID = ${id}. Quizas la Ficha no se encontro!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "No se puede borrar la Ficha con ID = " + id,
      });
    });
};

// Borrar todas las Fichas de la db
exports.deleteAll = (req, res) => {
  Ficha.destroy({
    where: {},
    truncate: false,
  })
    .then((nums) => {
      res.send({ message: `${nums} Fichas se han borrado con exito!` });
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Ocurrio un error mientras se borraban las fichas.",
      });
    });
};

// Encontrar todas las Fichas Completadas
exports.findAllCompleted = (req, res) => {
  Ficha.findAll({ where: { completed: true } })
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Ocurrio un error buscando las Fichas.",
      });
    });
};
