module.exports = (sequelize, Sequelize) => {
  const Ficha = sequelize.define("ficha", {
    full_name: {
      type: Sequelize.STRING
    },
    enter_name: {
      type: Sequelize.STRING
    },
    email: {
      type: Sequelize.STRING
    },
    pnumber: {
      type: Sequelize.STRING
    },
    category: {
      type: Sequelize.STRING
    },
    note: {
      type: Sequelize.STRING
    },
    completed: {
      type: Sequelize.BOOLEAN
    }
  });

  return Ficha;
};
