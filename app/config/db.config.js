module.exports = {
  HOST: "localhost",
  USER: "api_db",
  PASSWORD: "api_db",
  DB: "api_db",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }
};
